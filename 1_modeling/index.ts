import { randomUUID } from 'crypto'
import cron from 'node-cron'
import { StaticPool } from 'node-worker-threads-pool'

interface ITask {
  id: string // task id
  date: Date // task date
}

class TaskRepositoryAPIService {
  private TaskCreated: ITask[]

  constructor() {
    this.TaskCreated = []
  }

  pushTask(): void {
    const task: ITask = {
      id: randomUUID(),
      date: new Date(),
    }
    this.TaskCreated.push(task)
  }

  pullTask(): ITask {
    const task = this.TaskCreated.pop()
    return task
  }
}

const taskRepositoryService = new TaskRepositoryAPIService()

const workerPool = new StaticPool({
  size: 3,
  task() {
    let task = taskRepositoryService.pullTask()
    console.log(task)
  },
})

function loop() {
  cron.schedule('*/10 * * * * *', () => {
    taskRepositoryService.pushTask()
  })
  cron.schedule('*/2 * * * * *', () => {
    workerPool.exec()
  })
}

loop()
