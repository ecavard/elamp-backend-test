# eLamp Tech Test

You will find 3 directories with different tests you have to pass. The idea behind those tests is to evaluate how you're making code :

- **1_modeling** : Model and code structure
- **2_processing**: how you handle memory and computation
- **3_algo** : self-described !
