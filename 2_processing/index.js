"use strict";
exports.__esModule = true;
var ReviewService;
(function (ReviewService) {
    var ReviewAPIService = /** @class */ (function () {
        function ReviewAPIService() {
            this.ReviewCreated = [];
        }
        ReviewAPIService.prototype.createReview = function (ReviewData) {
            if (Math.random() <= 0.1) {
                return Promise.reject(new Error('Bad Gateway'));
            }
            else {
                this.ReviewCreated.push(ReviewData);
                return Promise.resolve(ReviewData);
            }
        };
        return ReviewAPIService;
    }());
    ReviewService.ReviewAPIService = ReviewAPIService;
})(ReviewService || (ReviewService = {}));
var AssessmentService;
(function (AssessmentService) {
    var AssessmentBulkService = /** @class */ (function () {
        function AssessmentBulkService() {
            this.ReviewBulk = [];
        }
        AssessmentBulkService.prototype.convertInput = function (input) {
            var _this = this;
            input.evaluations.forEach(function (evaluation) {
                _this.ReviewBulk.push({
                    userId: input.evaluatedUser,
                    authorId: evaluation.evaluator,
                    skillId: evaluation.skill,
                    comment: evaluation.comment
                });
            });
        };
        return AssessmentBulkService;
    }());
    AssessmentService.AssessmentBulkService = AssessmentBulkService;
})(AssessmentService || (AssessmentService = {}));
function generateData(dataCount) {
    if (dataCount === void 0) { dataCount = 10000; }
    var inputData = [];
    var NB_EVALUATED_USERS = 5; // limit to 5 evaluated users
    for (var j = 0; j < NB_EVALUATED_USERS; j++) {
        var evaluatedUser = 'U' + j;
        var evaluations = [];
        for (var i = 0; i < dataCount / NB_EVALUATED_USERS; i++) {
            evaluations.push({
                evaluator: 'U1' + i,
                skill: 'SKI' + i,
                comment: 'comment ' + i
            });
        }
        // an inputData contains the evaluatedUser and a list of evaluations
        // to create Review from evaluations : evaluatedUser <=> Review.user, evaluations[*].evaluator <=> Review.author, evaluations[*].skill <=> Review.skill, evaluations[*].comment <=> validaton.comment
        inputData.push({
            evaluatedUser: evaluatedUser,
            evaluations: evaluations
        });
    }
    return inputData;
}
function execBulkReviewCreator() {
    var inputData = generateData();
    var assessmentService = new AssessmentService.AssessmentBulkService();
    var reviewService = new ReviewService.ReviewAPIService();
    for (var _i = 0, inputData_1 = inputData; _i < inputData_1.length; _i++) {
        var input = inputData_1[_i];
        // TODO: create Review Data from Assessment Data
        assessmentService.convertInput(input);
        // TODO: secure this API call to create Review
        // reviewService.createReview(convertedInput)
        assessmentService.ReviewBulk.forEach(function (convertedInput, index) {
            reviewService
                .createReview(convertedInput)
                .then(function () {
                assessmentService.ReviewBulk = assessmentService.ReviewBulk.splice(index, 1);
            })["catch"](function (err) { });
        });
    }
}
execBulkReviewCreator();
