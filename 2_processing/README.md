# Safe Processing

## Introduction

As we are architectured in microservices, some services heavily communincate through either RPC, Event, or HTTP to other services.

We have an Assessment Service that creates a bunch of API requests to another service called Review Service at the end of a process. We have to process those API requests safely

The process is the one below :

- End-user starts an Assessment Campaign in which each user involved has to evaluate their colleagues/managers
- At the end of the process, end-user terminates the Assessment Campaign : it start a bulk Review Creation process in background where each user receives SkillReview for each assessment made by their colleagues.

## Goals

Try to set a Bulk SkillReview Creation process that safely make API Requests to SkillReview Service following those rules :

- SkillReview Service is represented as a function that returns a Promise and randomly fails 10% of the time
- If Review fails, the Bulk Review Creator must be replayable at any time without duplicating already processed Skill Review (users can only have one Skill Review per user in an Assessment Process)

## Scope

- Try to keep coding in NodeJS. If another language is needed (and arguments for this are provided) try implementation in another language.
- It has to run on environment with low memory limits (~400Mo RAM)
- Maximum Skill Review count is fixed to 50000
- You can modify predefined Interfaces (that represent Domain Objects in Services) if it increase the reliability of the process.
